#include "solution.h"

void unparse_solution(const Solution& solution, const string& filename)
{
   ofstream output;
   output.open(filename);

   if(!output.is_open())
       cerr << "Error while opening " << filename << endl;

   unparse_solution(solution, output);
   output.close();
}

void unparse_solution(const Solution& solution, ostream &output)
{
    output << solution.size() << endl;
    for(auto schedule : solution)
    {
        output << schedule.first << endl;
        output << schedule.second.size() << endl;
        for(auto rue : schedule.second)
            output << rue.first << ' ' << rue.second << endl;
    }
}

Solution parse_solution(const string& filename)
{
    //Ouverture du fichier
    ifstream input;
    input.open(filename);
    if(!input.is_open())
        cerr << "Error while onpening " << filename << endl;

    Solution solution = parse_solution(input);
    input.close();

    return solution;
}

Solution parse_solution(istream& input)
{
    //Parsing
    uint I;
    input >> I;

    //On initialise une solution à I intersections
    Solution solution(I);
    //Pour chaque intersection
    for(int i = 0; i < I; i++)
    {
        //Indice et nombre de feus
        uint ind, n;
        input >> ind >> n;

        solution[i] = {ind, vector<feu>(n)};

        for(int f = 0; f < n; f++)
        {
            //Nom de rue et durée du feu
            string rue;
            uint duree;
            input >> rue >> duree;

            solution[i].second[f] = {rue, duree};
        }
    }

    return solution;
}
