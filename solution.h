#ifndef SOLUTION_H
#define SOLUTION_H

#include <iostream>
#include <fstream>

#include "types.h"

void unparse_solution(const Solution& solution, const string& filename);
void unparse_solution(const Solution& solution, ostream& output);

Solution parse_solution(const string& filename);
Solution parse_solution(istream& file);

#endif // SOLUTION_H
