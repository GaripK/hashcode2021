#include "probleme.h"

void unparse_probleme(const Probleme &probleme, const string &filename)
{
    ofstream output;
    output.open(filename);

    if(!output.is_open())
        cerr << "Error while opening " << filename << endl;

    return unparse_probleme(probleme, output);
}

void unparse_probleme(const Probleme &probleme, ostream &output)
{
    output << probleme.D << ' ' << probleme.I << ' '
           << probleme.S << ' ' << probleme.V << ' '
           << probleme.F << endl;

    for(auto rue : probleme.rues)
        output << rue.second.B << ' ' << rue.second.E << ' '
               << rue.first << ' ' << rue.second.L << endl;
    for(auto chemin : probleme.chemins)
    {
        output << chemin.size() << ' ';
        for(auto nom_rue : chemin)
            output << nom_rue << ' ';
        output << endl;
    }
}

Probleme parse_probleme(const string &filename)
{
    ifstream input;
    input.open(filename);

    if(!input.is_open())
        cerr << "Error while opening " << filename << endl;

    Probleme probleme = parse_probleme(input);
    input.close();
    return probleme;
}

Probleme parse_probleme(istream &input)
{
    uint D, I, S, V, F;
    input >> D >> I >> S >> V >> F;
    Probleme probleme{D, I, S, V, F,
                      Rues(),
                      vector<chemin>(V)};

    for(int i = 0; i < S; i++)
    {
        uint B, E, L;
        string nom;
        input >> B >> E >> nom >> L;
        probleme.rues[nom] = {B, E, L};
    }

    for(int i = 0; i < V; i++)
    {
        uint P;
        input >> P;

        probleme.chemins[i] = vector<nom_rue>(P);
        for(int j = 0; j < P; j++)
            input >> probleme.chemins[i][j];
    }

    return probleme;
}
