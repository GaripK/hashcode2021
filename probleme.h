#ifndef PROBLEME_H
#define PROBLEME_H

#include <iostream>
#include <fstream>

#include "types.h"

void unparse_probleme(const Probleme& probleme, const string& filename);
void unparse_probleme(const Probleme &probleme, ostream& output);

Probleme parse_probleme(const string& filename);
Probleme parse_probleme(istream& input);

#endif // PROBLEME_H

