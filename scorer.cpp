#include "scorer.h"

uint scorer(Probleme &probleme, const Solution &solution)
{
    /* INITIALISATION */
    uint score = 0;
    //Les files de chaque rue
    Files files;
    for(auto rue : probleme.rues)
        files.insert({rue.first, File()});
    //Les positions de chaque voiture
    Positions positions;
    for(int i = 0; i < probleme.V; i++)
    {
        nom_rue nom = probleme.chemins[i][0];
        positions[i] = {0, probleme.rues[nom].L-1, true};
        files[nom].push(i);
    }
    //La durée de chaque schedule
    vector<uint> sched_durees(solution.size());
    for(int i = 0; i < solution.size(); i++)
    {
        sched_durees[i] = 0;
        for(int j = 0; j < solution[i].second.size(); j++)
            sched_durees[i] += solution[i].second[j].second;
    }

    /* LA MATRICE */
    //cout << "Temps de simulation : " << probleme.D << endl;
    for(int t = 0; t <=  probleme.D; t++)
    {
        //cout << endl << "TEMPS " << t << endl << endl;
        //On gère chaque voiture : toutes les voitures avancent ou se mettent en file
        for(auto v = positions.begin(); v != positions.end();)
        {
            //cout << "Voiture " << v->first << " : étape " << v->second.etape << '(' << probleme.chemins[v->first][v->second.etape] << "), position " << v->second.pos << ", en attente : " << v->second.enfile << endl;

            if(v->second.enfile){
                v++;
                continue;
            }

            //Sinon, elle avance
            v->second.pos += 1;

            //Si elle arrive au bout de la rue
            if(v->second.pos >= probleme.rues[probleme.chemins[v->first][v->second.etape]].L - 1)
            {
                //Soit elle a fini sa course et on score
                if(v->second.etape == probleme.chemins[v->first].size()-1)
                {
                    positions.erase(v++);
                    score += probleme.F + probleme.D - t ;
                //Soit elle fait la file au feu
                } else {
                    files[probleme.chemins[v->first][v->second.etape]].push(v->first);
                    v->second.enfile = true;
                    v++;
                }
            } else {
                v++;
            }
        }

        //cout << endl;
        //On gère tous les feux : tous les feux verts font avancer leur première voiture
        //Du coup, pour chaque intersection
        for(int i = 0; i < solution.size(); i++)
        {
            //La position en temps dans le schedule
            uint sched_t = (t) % sched_durees[i];
            //Quelle rue est verte ?
            nom_rue nom;
            for(int j = 0; j < solution[i].second.size(); j++)
            {
                if(sched_t < solution[i].second[j].second)
                    nom = solution[i].second[j].first;
                sched_t -= solution[i].second[j].second;
            }
            //cout << "Intersection " << i << ", feu vert : " << nom << endl;
            //Quelle voiture doit donc avancer (le cas échéant T_T)?
            if(!files[nom].empty()){
                uint v = files[nom].front();
                files[nom].pop();

                //cout << "La voiture " << v << " avance" << endl;

                //Qu'elle avance alors
                positions[v] = {positions[v].etape+1, -1, false};
            }
        }

    }

    return score;
}
