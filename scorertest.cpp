#include "probleme.h"
#include "solution.h"
#include "scorer.h"

int main(int argc, char** argv)
{
    if(argc != 3)
    {
        cerr << "usage: " << argv[0] << " <probleme> <solution>" << endl;
        exit(1);
    }

    Probleme probleme = parse_probleme(argv[1]);
    Solution solution = parse_solution(argv[2]);

    uint score = scorer(probleme, solution);
    cout << "Score : " << score << endl;
}
