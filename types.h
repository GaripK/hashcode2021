#ifndef TYPES_H
#define TYPES_H

#include<string>
#include <vector>
#include <map>

using namespace std;

//Solution
using nom_rue = string;
using feu = pair<nom_rue, uint>;
using schedule = pair<uint, vector<feu>>;
using Solution = vector<schedule>;

//Problème
struct Rue{
    uint B, E, L;
};

using Rues = map<nom_rue, Rue>;

using chemin = vector<nom_rue>;
using Chemins = vector<chemin>;

struct Probleme{
    uint D, I, S, V, F;
    Rues rues;
    Chemins chemins;
};

#endif // TYPES_H
