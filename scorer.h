#ifndef SCORER_H
#define SCORER_H

#include <queue>
#include <iostream>

#include "types.h"

using File  = queue<uint>;
using Files = map<nom_rue, File>;

struct Position{
    uint  etape;
    int    pos;
    bool  enfile;
};

using Positions = map<uint, Position>;

uint scorer(Probleme& probleme, const Solution& solution);

#endif // SCORER_H
